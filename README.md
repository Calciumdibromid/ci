# CI Images

This repo contains all images used in the ci pipeline of CaBr2.
They are built using [woodpecker](.woodpecker/) and uploaded to the [registry at Codeberg](https://codeberg.org/Calciumdibromid/-/packages).

## Images

- [Rust and Angular pipeline](pipeline/)
