FROM ubuntu:jammy

ENV WKHTMLTOX_VERSION=0.12.6.1-2
ENV NODE_VERSION=18

RUN apt-get update \
    && apt-get install tzdata -y --quiet \
    && apt-get install keyboard-configuration -y --quiet \
    # install build dependencies
    && apt-get install -y --quiet libwebkit2gtk-4.0-dev build-essential curl wget file libssl-dev libgtk-3-dev libayatana-appindicator3-dev librsvg2-dev ca-certificates gnupg \
    && rm -rf /var/lib/apt/lists/*

# install wkhtmltopdf lib + header
RUN curl -L -o /tmp/wkhtmltox_amd64.deb https://github.com/wkhtmltopdf/packaging/releases/download/$WKHTMLTOX_VERSION/wkhtmltox_$WKHTMLTOX_VERSION.jammy_amd64.deb \
    && apt-get update \
    && apt-get install /tmp/wkhtmltox_amd64.deb -y \
    && rm /tmp/wkhtmltox_amd64.deb \
    && rm -rf /var/lib/apt/lists/*

# install node and yarn
RUN mkdir -p /etc/apt/keyrings \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_VERSION.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
    && curl -fsSL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor -o /etc/apt/keyrings/yarnpkg.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/yarnpkg.gpg] https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarnpkg.list \
    && apt-get update \
    && apt-get install nodejs yarn -y \
    && rm -rf /var/lib/apt/lists/*

ENV src_root="/woodpecker/src/codeberg.org/Calciumdibromid/CaBr2/"
RUN mkdir -p $src_root

# install rust toolchain + needed binaries
RUN useradd --uid 1001 --create-home runner
RUN chown runner: $src_root
USER runner
ENV CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal --component clippy rustfmt --target wasm32-unknown-unknown
ENV PATH="/home/runner/.cargo/bin:${PATH}"
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

# download and cache dependencies
COPY pipeline/cache_build_deps.sh /
RUN ./cache_build_deps.sh
