#!/bin/bash

src_paths=("crates/config" "crates/error-ser" "crates/load_save" "crates/logger" "crates/search" "crates/types")

for ((i=0;i<=${#src_paths[@]}-1;i++)); do
    src="${src_paths[i]}"

    cd $src_root

    cargo new --lib $src
    cd $src
    curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/Cargo.toml" -o Cargo.toml
done


src_paths=("webserver" "frontend/src-tauri")

for ((i=0;i<=${#src_paths[@]}-1;i++)); do
    src="${src_paths[i]}"

    cd $src_root

    cargo new $src
    cd $src
    curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/Cargo.toml" -o Cargo.toml
    curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/Cargo.lock" -o Cargo.lock
done


cd $src_root

src="frontend/src-wasm"

cargo new --lib $src
cd $src
curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/Cargo.toml" -o Cargo.toml
curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/Cargo.lock" -o Cargo.lock

cargo fetch --target wasm32-unknown-unknown


cd $src_root

curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/Cargo.toml" -o Cargo.toml
curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/Cargo.lock" -o Cargo.lock

cargo fetch


src="frontend"

cd $src

curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/package.json" -o package.json
curl -fs "https://codeberg.org/Calciumdibromid/CaBr2/raw/branch/develop/${src}/yarn.lock" -o yarn.lock

yarn install --pure-lockfile


rm -rf $src_root || true
