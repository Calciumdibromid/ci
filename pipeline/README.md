# CI

Image for CI (test & build) for the CaBr2 project.

## Installed Software

- yarn and Nodejs
- Rust compiler with clippy and rustfmt
- wasm-pack
- wkhtmltox
- everything needed for Tauri
